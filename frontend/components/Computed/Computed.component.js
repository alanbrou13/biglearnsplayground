import Vue from 'vue';
import Component from 'vue-class-component';

@Component({})
export default class Computed extends Vue {
    message = 'should be initialized';

    /**
     * Execute when message change
     * @returns {string}
     */
    get reversedMessage() {
        return this.message.split('').reverse().join('');
    }
}
