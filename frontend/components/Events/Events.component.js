import Vue from 'vue';
import Component from 'vue-class-component';

@Component({})
export default class Events extends Vue {

    /**
     * Say hi.
     */
    sayHi () {
        alert('hi');
    }

    /**
     * Show the event.
     * @param event
     */
    getEvent (event) {
        alert(event);
    }

}
