import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
    props:{
        fatherText:{
            type:String,
            default: false,
        }
    }
})
export default class Child extends Vue {
}
