import Vue from 'vue';
import Component from 'vue-class-component';

@Component()
export default class Lifecycle extends Vue {

    prop = 'Blank';

    /**
     * Data has not been made reactive, and events have not been set up yet.
     */
    beforeCreate() {
        alert('Nothing gets called before me!');
    }

    /**
     * Data and events are registered but the component is not rendered and included in the DOM.
     */
    created() {
        this.prop = 'Example property update.';
        alert('propertyComputed will update, as this.prop is now reactive.')
    }

    /**
     * Defore the component is included in the DOM, the template and the functions are compiled.
     */
    beforeMount() {
        alert(`this.$el doesn't exist yet, but it will soon!`)
    }

    /**
     * Component rendered, here you have access to all stuff inside this component.
     */
    mounted() {
        alert(this.$el.textContent) // I'm text inside the component.
    }
}
