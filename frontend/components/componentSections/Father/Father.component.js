import Vue from 'vue';
import Component from 'vue-class-component';
import Child from '../Child';
import ComponentA from '../DynamicComponentA';
import ComponentB from '../DynamicComponentB';
import Lifecycle from '../Lifecycle';

@Component({
    components: {
        Child,
        ComponentA,
        ComponentB,
        Lifecycle,
    }
})
export default class Father extends Vue {
    messageToChild='';
    selectedComponent = null;
    showLifeHistory = false;

    /**
     * Get the event from the child.
     */
    handleChildEvent(){
        alert('My child say hi');
    }

    /**
     * Change the component to show.
     * @param componentToDisplay
     */
    changeComponent(componentToDisplay){
        this.selectedComponent = componentToDisplay;
    }

    /**
     * Hide/Show component.
     */
    showLifecycle(){
        if (this.showLifeHistory) {
            this.showLifeHistory = false;
        } else {
            this.showLifeHistory = true;
        }
    }
}
