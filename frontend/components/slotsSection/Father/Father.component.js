import Vue from 'vue';
import Component from 'vue-class-component';
import Child from '../../slotsSection/Child';


@Component({
    components: {Child}
})
export default class Father extends Vue {
    get itemsArray() {
        return [ {name: 'Homer', age: '15'}, {name: 'Men', age: '22'} ];
    }
    
}
