import Vue from 'vue';
import Component from 'vue-class-component';

@Component({})
export default class Directives extends Vue {

    canShow = false;
    days = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' ];
    personObject = [ {name: 'Homer', age: '15'}, {name: 'Men', age: '22'} ];
    dynamicCssClassA = false;
    dynamicCssClassB = true;
    message = '';

    /**
     * Hide/show the text.
     */
    showText () {
        if (this.canShow) {
            this.canShow = false;
        } else {
            this.canShow = true;
        }
    }

}
