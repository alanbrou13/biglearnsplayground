import Vue from 'vue';
import Router from 'vue-router';
import Home from 'Components/Home';
import Directives from 'Components/Directives';
import Events from 'Components/Events';
import Computed from 'Components/Computed';
import Father from 'Components/componentSections/Father';
import SlotFather from 'Components/slotsSection/Father';

Vue.use(Router);

const AppRouter = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        }, {
            path: '/directives/',
            name: 'Directives',
            component: Directives
        }, {
            path: '/events/',
            name: 'Events',
            component: Events
        }, {
            path: '/computed/',
            name: 'Computed',
            component: Computed
        }, {
            path: '/components/',
            name: 'Components',
            component: Father
        },
        {
            path: '/slots/',
            name: 'Slots',
            component: SlotFather
        }
    ]
});

export default AppRouter;
